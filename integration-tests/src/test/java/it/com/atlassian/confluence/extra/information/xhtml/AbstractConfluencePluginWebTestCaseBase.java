package it.com.atlassian.confluence.extra.information.xhtml;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;
import com.atlassian.confluence.plugin.functest.helper.SpaceHelper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public abstract class AbstractConfluencePluginWebTestCaseBase extends AbstractConfluencePluginWebTestCase {
    long createSpace(String spaceKey, String title, String description) {
        SpaceHelper spaceHelper = getSpaceHelper();
        spaceHelper.setKey(spaceKey);
        spaceHelper.setName(title);
        spaceHelper.setDescription(description);
        assertTrue(spaceHelper.create());

        return spaceHelper.getHomePageId();
    }

    void deleteSpace(String spaceKey) {
        SpaceHelper spaceHelper = getSpaceHelper(spaceKey);
        assertTrue(spaceHelper.delete());
    }

    long createPage(String spaceKey, String title, String content) {
        return createPage(spaceKey, 0, title, content, new ArrayList());
    }

    @SuppressWarnings("unchecked")
    long createPage(String spaceKey, long parentId, String title, String content, List labels) {
        PageHelper helper = getPageHelper();

        helper.setSpaceKey(spaceKey);
        helper.setParentId(parentId);
        helper.setTitle(title);
        helper.setContent(content);
        helper.setCreationDate(new Date());
        helper.setLabels(labels);
        assertTrue(helper.create());

        // return the generated id for the new page
        return helper.getId();
    }

    /**
     * Views the page for a Page or BlogPost (AbstractPage)
     *
     * @param entityId the ID of the AbstractPage
     */
    void viewPageById(long entityId) {
        gotoPage("/pages/viewpage.action?pageId=" + entityId);
    }

    /**
     * Views the content editor for a Page or BlogPost (AbstractPage)
     *
     * @param entityId the ID of the AbstractPage
     */
    void editPageById(long entityId) {
        gotoPage("/pages/editpage.action?pageId=" + entityId);
    }
}
